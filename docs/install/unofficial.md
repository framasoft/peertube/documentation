# Unofficial installations

## Git

::: warning
This guide assumes that you have read the instructions in the [official installation guide](./any-os.md)
and thus omits some configurations steps (reverse proxy, database configuration, etc.). At
the end you should have a PeerTube platform running the latest development version, which can
be considered unstable.
:::

First, go to the Peertube folder and switch to the Peertube user.

```bash
cd /var/www/peertube/versions/
sudo -u peertube -H bash
```

Then clone the git repository.

```bash
git clone https://github.com/Chocobozzz/PeerTube.git peertube-develop
cd peertube-develop/
```

It should automatically be on the `develop` branch, which you can verify with `git branch`. You can
also switch to another branch or a specific commit with `git checkout [branch or commit]`. Once you
have the correct version, run the build:

```bash
yarn install --pure-lockfile
npm run build
```

The compilation will take a long time. You can also run it on your local computer, and transfer the
entire folder to your server.

Now you should make sure to add any new config fields to your `production.yaml`. And you should make
a backup of the database:

```bash
SQL_BACKUP_PATH="backup/sql-peertube_prod-$(date -Im).bak" && \
     cd /var/www/peertube && sudo -u peertube mkdir -p backup && \
     sudo -u postgres pg_dump -F c peertube_prod | sudo -u peertube tee "$SQL_BACKUP_PATH" >/dev/null
```

Finally, update the `peertube-latest` symlink to point at the new version:

```bash
cd /var/www/peertube && \
    sudo unlink ./peertube-latest && \
    sudo -u peertube ln -s versions/peertube-develop ./peertube-latest
```

Now you just need to restart Peertube. With systemd, just run `sudo systemctl restart peertube`.

Do not try to upgrade from one development version to another by running `git pull` and `npm run build`. This
will break your website. Either switch back to a release version, or make a copy of the `peertube-develop`
folder and run the compilation there.

## ArchLinux

On Arch Linux, PeerTube can be installed through the Arch User Repository thanks to a __community package__ made by [daftaupe](https://aur.archlinux.org/packages/peertube/).

```sh
asp checkout peertube
cd peertube
makepkg --syncdeps --rmdeps --install --clean
```

Or

```sh
yay -S peertube
```

#### Configuration

You now have to [configure the database](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#database)
and credentials to it in the configuration file of PeerTube
in `/usr/share/webapps/peertube/config/production.yaml`.

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no Arch packages available for RC or nightly builds of PeerTube. Please use the tarball:
{% include_relative installations/tarball.md %}
</div>


## CentOS

On CentOS, Fedora and RHEL, you can install PeerTube via a __community package__ made by [daftaupe](https://copr.fedorainfracloud.org/coprs/daftaupe/peertube/) on COPR.

```sh
dnf copr enable daftaupe/peertube
```

#### Prerequisites

You will need PostgreSQL, Node.JS and FFMpeg :

* Fedora you need RPM Fusion repository enabled
  ```sh
  sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
  ```
* CentOS you will need EPEL and the unofficial EPEL-multimedia repositories enabled
  ```sh
  cd /etc/yum.repos.d && curl -O https://negativo17.org/repos/epel-multimedia.repo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm https://negativo17.org/repos/epel-multimedia.repo
  ```

**Setup the database**

```sh
su - postgres
initdb
createuser peertube -W
createdb -O peertube peertube_prod
echo "host peertube_prod peertube 127.0.0.1/32 md5" >> data/pg_hba.conf
```

```sh
systemctl reload postgresql
```

**Start the services**

```sh
systemctl start redis
```

**Edit the configuration to fit your needs**

```sh
vim /etc/peertube/production.yaml
```

**Start PeerTube and get the initial root / password**

```sh
systemctl start peertube && journalctl -f -u peertube
```

## YunoHost

On Debian running YunoHost, you can install Yarn, Node and PeerTube in one shot via a __community package__.

[![Install Peertube with YunoHost](https://install-app.yunohost.org/install-with-yunohost.png)](https://install-app.yunohost.org/?app=peertube)

See [here](https://github.com/YunoHost-Apps/peertube_ynh) for support.

## Kubernetes

You can deploy peertube on [Kubernetes](https://kubernetes.io/) using the
[official docker image](/install/docker).

Two main options are available :

* Using [Helm](https://helm.sh/) with
  [peertube-helm](https://git.lecygnenoir.info/LecygneNoir/peertube-helm) which is
  initially forked from [jsl-helmcharts](https://gitlab.com/jsl3/helmcharts/-/tree/master/peertube)

* Using Kubernetes deployments which supports storing content on [S3 remote storage](/maintain/remote-storage.md):
  [peertube-on-kubernetes](https://forge.extranet.logilab.fr/open-source/peertube-on-kubernetes)

* Using Kubernetes and [Kustomize](https://kustomize.io/) with Native Object storage
  [peertube-k8s](https://github.com/coopgo/peertube-k8s)


## Shell and Docker Compose

A quick way to install the [official Docker stack](/install/docker) of peertube **by one shell command** on a **bare server** with only Docker installed.

Features:

- install or upgrade of [Docker Compose](https://docs.docker.com/compose)
- build stack tree and official config files in `/var/peertube`
- automatic fill of variables in `.env` with official template `MY_EMAIL_ADDRESS` and `MY_DOMAIN`
- generate PostgreSQL crendentials
- generate first Let's Encrypt certificate with [Certbot](https://certbot.eff.org/) container
- create systemd service
- run the stack

Also exposes the [CLI Server Tools](/maintain/tools#server-tools) and more for maintenance like:
- PostgreSQL tasks: dump, restore, psql requests
- nginx control
- stack upgrade with very-small downtime
- etc.

Project Link: https://github.com/kimsible/getpeer.tube

::: warning
Most these projects are in beta, so feedback is welcome to improve this type of deployment.
:::

## Ansible Playbook for Fedora & Fedora Server

You can install Peertube on Fedora / Fedora Server using this Ansible playbook. 

PeerTube server will be run via Podman and Systemd quadlets. 

Playbook link: https://forge.nuculabs.de/dnutiu/ansible-playbooks/src/branch/master/peertube-server

Note: It assumes that you will bring your own web server & mail service provider. The playbook will take care of SELinux, Firewalld, Podman Containers, Backup strategy and Systemd quadlets

You will have to edit the variable defined in variables.yaml file as follows:

```yaml
# Set this to true if you want to mount SAMBA volumes inside containers.
selinux_virt_use_samba: true
# Base data directory
base_directory: "/peertube"
peertube:
  # It will open ports 9000 and 1935. If you have the webserver on the same machine you should disable this.
  open_firewall_ports: true
  env_file: "environment_file"
  backups_directory: "/peertube/backups"
  image_name: "docker.io/chocobozzz/peertube:v7.0.1-bookworm"
  data_directory: "/peertube/data"
  config_directory: "/peertube/config"
  environment:
    # replace with: openssl rand -hex 32
    secret: "cff9ea5880b72f16d426059e0f3182ca478c580304967f476f242e583b06173f"
    db_user: "user"
    db_pass: "pass"
    webserver:
      hostname: "peertube-testing.local" # replace with your domain
      port: 80 # use 443 for tls
      tls: "false" # set to true if using https
      trust_proxy: '["127.0.0.1", "loopback", "192.168.0.0/24", "172.18.0.0/16", "10.0.0.0/16", "173.245.48.0/20", "103.21.244.0/22","103.22.200.0/22","103.31.4.0/22","141.101.64.0/18","108.162.192.0/18","190.93.240.0/20","188.114.96.0/20","197.234.240.0/22","198.41.128.0/17","162.158.0.0/15","104.16.0.0/13","104.24.0.0/14","172.64.0.0/13","131.0.72.0/22"]'
    smtp:
      user: ""
      pass: ""
      email: "no-reply@peertube.local"
      hostname: "smtp.gmail.com" # or any other smtp service
postgres:
  postgres_directory: "/peertube/postgres"
  image_name: "docker.io/postgres:13-alpine"
redis:
  image_name: "docker.io/redis:6-alpine"
```

Then setup which servers to install peertube on by replacing the `inventory.ini` file as folows:

```
# cat inventory.ini
[servers]
192.X.X.X ansible_user=root
```

Edit the hosts in the `playbook.yaml`:

```yaml
- name: Install PeerTube
  hosts: servers
  become: true
  become_method: sudo
  vars_files:
    - variables.yaml
[...]
```

And run the playbook with: 

```
make run
```

Note: If you don't have ansible installed in your system you can install dependencies by running `make install` or install Ansible using your distro's package manager:

```
sudo dnf install ansible
ansible-galaxy collection install community.general
ansible-galaxy collection install containers.podman
ansible-galaxy collection install ansible.posix
```

Once the playbook has finished it you will have a PeerTube server up & running and simple file system level backups that are executed daily. 

Backups will stop the PostgreSQL db server when running. You can stop automatic backups by disabling the back-up timer.

```shell
systemctl stop peertube-backup.timer
systemctl disable peertube-backup.timer
```

You can also modify the backup script to your liking by [editing it](https://forge.nuculabs.de/dnutiu/ansible-playbooks/src/branch/master/peertube-server/templates/backup/backup.sh.j2).
