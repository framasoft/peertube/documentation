<div class="index-page">

<h1 align="center">
  <a href="https://joinpeertube.org" target="_blank">
    <img class="brand-img" src="/brand.png" alt="PeerTube">
  </a>
</h1>

<p align=center>
  <strong><a href="https://joinpeertube.org">Website</a></strong>
  | <strong><a href="https://joinpeertube.org/instances">Join an instance</a></strong>
  | <strong><a href="https://github.com/Chocobozzz/PeerTube/blob/develop/README.md#package-create-your-own-instance">Create a PeerTube platform</a></strong>
  | <strong><a href="https://github.com/Chocobozzz/PeerTube/blob/develop/README.md#contact">Chat with us</a></strong>
  | <strong><a href="https://framasoft.org/en/#soutenir">Donate</a></strong>
</p>

<p align="center">
Be part of a network of multiple small federated, interoperable video hosting providers. Follow video creators and create videos. No vendor lock-in. All on a platform that is community-owned and ad-free.
</p>

<p align="center">
  <strong>Developed with &#10084; by <a href="https://framasoft.org">Framasoft</a></strong>
</p>

<p align="center">
  <a href="https://framasoft.org" target="_blank">
    <img width="150px" src="/assets/logo/framasoft.png" alt="Framasoft logo"/>
  </a>
</p>

<br />

<p align="center">
  <a href="https://framatube.org/videos/watch/217eefeb-883d-45be-b7fc-a788ad8507d3" target="_blank">
    <img src="/assets/landing-page.jpg" alt="screenshot" />
  </a>
</p>

Introduction
----------------------------------------------------------------

PeerTube is a free, decentralized and federated video platform developed as an alternative to other platforms that centralize our data and attention, such as YouTube, Dailymotion or Vimeo. :clapper:

To learn more:
* This [two-minute video](https://framatube.org/videos/watch/217eefeb-883d-45be-b7fc-a788ad8507d3) (hosted on PeerTube) explaining what PeerTube is and how it works
* PeerTube's project homepage, [joinpeertube.org](https://joinpeertube.org)
* Demonstration platforms:
  * [peertube.cpy.re](https://peertube.cpy.re) (stable)
  * [peertube2.cpy.re](https://peertube2.cpy.re) (Nightly)
  * [peertube3.cpy.re](https://peertube3.cpy.re) (RC)
* This [video](https://peertube.cpy.re/videos/watch/da2b08d4-a242-4170-b32a-4ec8cbdca701) demonstrating the communication between PeerTube and [Mastodon](https://github.com/tootsuite/mastodon) (a decentralized Twitter alternative)


:raised_hands: Contributing
----------------------------------------------------------------

You don't need to be a programmer to help!

You can give us your feedback, report bugs, help us translate PeerTube, write documentation, and more. Check out the [contributing
guide](https://github.com/Chocobozzz/PeerTube/blob/develop/.github/CONTRIBUTING.md) to know how, it takes less than 2 minutes to get started. :wink:

You can also join the cheerful bunch that makes our community:

* Chat<a name="contact"></a>:
  * Matrix (bridged on IRC and [Discord](https://discord.gg/wj8DDUT)) : **[#peertube:matrix.org](https://matrix.to/#/#peertube:matrix.org)**
  * IRC : **[#peertube on irc.libera.chat:6697](https://web.libera.chat/#peertube)**
* Forum:
  * Framacolibri: [https://framacolibri.org/c/peertube](https://framacolibri.org/c/peertube)

Feel free to reach out if you have any questions or ideas! :speech_balloon:

:package: Create your own PeerTube platform
----------------------------------------------------------------

See the [production guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md), which is the recommended way to install or upgrade PeerTube. For hardware requirements, see [Should I have a big server to run PeerTube?](https://joinpeertube.org/faq#should-i-have-a-big-server-to-run-peertube) in the FAQ.

See the [community packages](https://docs.joinpeertube.org/install-unofficial), which cover various platforms (including [YunoHost](https://install-app.yunohost.org/?app=peertube) and [Docker](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/docker.md)).

:book: Documentation
----------------------------------------------------------------

If you have a question, please try to find the answer in the [FAQ](https://joinpeertube.org/faq) first.

### User documentation

See the [user documentation](https://docs.joinpeertube.org/use-setup-account).

### Admin documentation

See [how to create your own PeerTube platform](https://github.com/Chocobozzz/PeerTube/blob/develop/README.md#package-create-your-own-instance).

See the more general [admin documentation](https://docs.joinpeertube.org/admin-following-instances).

### Tools documentation

Learn how to import/upload videos from CLI or admin your PeerTube platform with the [tools documentation](https://docs.joinpeertube.org/maintain-tools).

### Technical documentation

See the [architecture blueprint](https://docs.joinpeertube.org/contribute-architecture) for a more detailed explanation of the architectural choices.

See our REST API documentation:
  * OpenAPI 3.0.0 schema: [/support/doc/api/openapi.yaml](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/api/openapi.yaml)
  * Spec explorer: [docs.joinpeertube.org/api-rest-reference.html](https://docs.joinpeertube.org/api-rest-reference.html)

See our [ActivityPub documentation](https://docs.joinpeertube.org/api-activitypub).

</div>
