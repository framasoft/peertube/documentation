# Customize your PeerTube platform

## Own logo, icons etc.

In the path `storage/client-overrides` create the folder `assets` and in this folder another one with the name `images`. If you followed the installation instructions the full path is `/var/www/peertube/storage/client-overrides/assets/images`.

There you have to place files with the same names like the ones in `client/dist/assets/images` e.g. add a `logo.svg` to replace the default logo or add a `favicon.png` to replace the default favicon.

You don't have recreate all files you can find in `client/dist/assets/images`. If you leave some of them out PeerTube will just fallback to the default ones (see also the explanation for `client_overrides` in the config file).

## Install a theme

You can install themes created by the PeerTube community in *the Left Menu* -> `Administration` section -> `Settings` -> `Plugins/Themes` -> `Search themes`.

![](/assets/search-themes.jpg)

Then, you can set your default platform theme in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Basic`.

Your users will be able to override this setting in their preference, so they can choose another theme among those you have installed.

**Learn how to create a theme: [https://docs.joinpeertube.org/contribute/plugins](/contribute/plugins)**

## Customize your platform CSS

If you just want to update some elements of your platform, you can also inject some CSS code configured in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Advanced`.

It's easy to change the main PeerTube colors using CSS variables using:

```css
body#custom-css {
  --primary: #E8252D;
}
```

See the [theme documentation](/contribute/plugins#css-variables) for an overview of the PeerTube variable system.


## Install a plugin

Administrators can install plugins to change the behaviour of their PeerTube application. Plugins can inject CSS, change the UI and the server logic.

A plugin could for example:
 * Hide some buttons (counters, menu entries...)
 * Reject specific comments or videos (depending on many criterias)
 * Put videos in quarantine
 * Add client analytics ([Matomo plugin](https://www.npmjs.com/package/peertube-plugin-matomo), etc.)
 * Add a CAPTCHA in the registration form (see [CAPTCHA plugins](https://www.npmjs.com/search?q=keywords%3Apeertube%20plugin%20captcha))
 * Block some IPs
 * Customize the player
 * Add auth methods to your PeerTube platform ([LDAP](https://framagit.org/framasoft/peertube/official-plugins/-/tree/master/peertube-plugin-auth-ldap), [OpenId](https://framagit.org/framasoft/peertube/official-plugins/-/tree/master/peertube-plugin-auth-openid-connect), etc.)
 * ...

You can install plugins created by the PeerTube community in *the Left Menu* -> `Administration` section -> `Settings` -> `Plugins/Themes` -> `Search plugins`.

![](/assets/search-plugins.jpg)

**Learn how to create a plugin: [https://docs.joinpeertube.org/contribute/plugins](/contribute/plugins)**

## Inject JavaScript in client

If you just want to update a particular client behaviour of your platform, you can also inject some JavaScript code configured in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Advanced`.
