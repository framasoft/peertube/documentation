# Configuration

Admins can configure their PeerTube platform directly in the web interface using, in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration`.

All the fields from this web interface will override your `production.yaml` configuration keys and are saved in a `local-production.json` file as  explained in the [system configuration documentation](/maintain/configuration).

## Basic

### Videos

  * **Allow users to upload a new version of their video**: <Badge type="info" text="PeerTube >= 6.0"></Badge> check the box to allow the user to replace the video file in the *Update Video* page
  * **Enable video transcription**: <Badge type="info" text="PeerTube >= 6.2"></Badge> Automatically create a subtitle file of uploaded/imported VOD videos

## VOD transcoding

Enabling transcoding ensures that your users will have a video playback that works. It's highly recommended to enable this.

### Web Video transcoding or HLS transcoding

We have two different ways to stream the video in the PeerTube player:

  * injecting a Web compatible video file in the `<video>` HTML tag
  * using [HLS with P2P](https://en.wikipedia.org/wiki/HTTP_Live_Streaming).

At the beginning of PeerTube, we only supported Web Video (previously known as "WebTorrent") streaming. Due to several limitations of the Web Video system, we had to add HLS with P2P support.
Unfortunately, we can't use the same video file for the two methods: we need to transcode 2 different versions of the file (a fragmented mp4 for HLS, and a raw mp4 for Web Videos).

So if you enable Web Videos **and** HLS, the storage will be multiplied by 2.

We recommend you to enable HLS (and disable Web Videos if you don't want to store 2 different versions of the same video resolution) because video playback in PeerTube web client is better:

 * Support P2P (using WebRTC) to exchange parts of the video with other users watching the same video to save server bandwidth
 * Support video redundancy by other PeerTube platforms
 * The player can adapt video resolution automatically
 * Video resolution change is smoother

### Resolutions

PeerTube can transcode the uploaded video in multiple resolutions. It allows to users that does not have a high speed Internet connection to watch the video in low quality. Keep in mind that a transcoding job takes a lot of CPU, requires time and create an additional video file stored on your disk storage.

You can disable *Also transcode original resolution* option if you want to limit the maximum resolution available on your PeerTube platform.

![](/assets/transcoding-resolutions.jpg)

### Type of files users can upload

If you enable transcoding, you can also allow additional files formats like `.mkv`, `.avi` and/or allow audio file uploads (PeerTube will create a video from them).

![](/assets/transcoding-additional-extensions.jpg)


## Live streaming

You can enable live streaming on your PeerTube platform in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Live Streaming`. In this section, you can choose to:

 * Allow your users to automatically save a replay of their live. Enabling VOD transcoding is required to enable this setting
 * Limit parallel lives per account/on your platform
 * Set a max duration for lives
 * Enable live transcoding. This setting is different than VOD transcoding, because PeerTube will transcode the live in real time.
 It means that if your platform is streaming 10 lives, PeerTube will run 10 FFmpeg processes (high CPU/RAM usage).
 So we recommend you to make some tests to see how many parallel lives your platform can handle before increasing limits/enabling transcoding resolutions

Bear in mind that enabling live streaming will make your server listen on port `1935/TCP` (and `1936/TCP` if RTMPS is enabled), which is required for incoming RTMP by your streamers.
The listening port can be changed in the PeerTube configuration file.

## Automatic transcription

Automatic transcription can be enabled in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration`. Transcription can be run by [PeerTube runners](/admin/remote-runners) too.
More configuration options, like the Whisper engine and/or model (by default PeerTube uses the `small` model), are available in the configuration file (`production.yaml`).

## Search

### Global search

You can enable global search, to use an external index (https://framagit.org/framasoft/peertube/search-index).
This way, you give the ability to your users to get results from PeerTube platforms that may not be federated with yours.

To enable global search, your need to specify the `Search index URL` of the remote search index.
Framasoft provides a search index (indexing videos from https://instances.joinpeertube.org): https://search.joinpeertube.org/.
This index is not moderated, so we strongly recommend that you create your own moderated index.


### URI search

Users can use a video URI or channel URI/handle search to fetch a specific remote video/channel, that may not be federated with your PeerTube platform.
Since this feature adds ability to your users to *escape* from your federation policy, you can disable it using
`Allow users to do remote URI/handle search` or `Allow anonymous to do remote URI/handle search` checkboxes.
