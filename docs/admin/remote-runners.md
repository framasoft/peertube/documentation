# Remote Runners

PeerTube can offload high CPU/long job like transcoding to a remote PeerTube runner.

When the remote runners option is enabled for a particular task, PeerTube stops processing jobs locally and waits for a remote runner to request, accept and process them. The runner finally post the job result to PeerTube.

  * A more complete architecture documentation is available on [the Architecture documentation](/contribute/architecture#remote-vod-live-transcoding).
  * A detailed documentation to configure a runner is available on [CLI -> PeerTube runner](/maintain/tools#peertube-runner).

## Enable remote runners

The following tasks can be processed by remote runners, if enabled:
 * [VOD transcoding](/admin/configuration#vod-transcoding) in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `VOD transcoding`
 * [Studio transcoding](/use/studio) in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `VOD transcoding`
 * [Live transcoding](/admin/configuration#live-streaming) in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Live streaming`
 * [Automatic transcription](/admin/configuration#automatic-transcription) in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Basic`


::: warning
Registered runners can alter video content. It's the reason why you must have confidence in their administrator.
:::

## Manage remote runners

When one of the remote runner options is enabled in the PeerTube platform configuration, a new administration menu entry is available to list registered runners: in *the Left Menu* -> `Administration` section -> `Runners` -> `Remote runners`.

![](/assets/remote-runners/list-remote-runners.jpg)

A runner needs a **Runner registration token** to register itself to your PeerTube platform.
To list available registration tokens, click on the **Runner registration tokens** button.

![](/assets/remote-runners/list-runner-registration-tokens.jpg)

You can generate multiple registration tokens and revoke them. Revoking a registration token will also unregister associated remote runners.

## Manage runner jobs

To list remote runner jobs, go in *the Left Menu* -> `Administration` section -> `Runners` -> `Runner jobs`.

![](/assets/remote-runners/list-remote-jobs.jpg)

You can see all remote jobs in the table. A job can be in the following state:
 * *Pending*: the job is waiting for a runner to be processed
 * *Processing*: the job is being processed by a remote runner
 * *Processing*: the job is being processed by a remote runner
 * *Completed*: the job has been processed by a remote runner
 * *Failed*: the job failed several times, so PeerTube decided to fail it
 * *Waiting for parent job*: the job is waiting for another job to finish before being available

A pending job can be cancelled.


You can also see the job content. The public payload is sent to the remote runner, and the private payload is only used internally by the PeerTube platform.

![](/assets/remote-runners/remote-job-content.jpg)

