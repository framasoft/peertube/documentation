# PeerTube platform follows & redundancy

## Platform follows

Following servers and being followed as a server ensure visibility of your videos on other PeerTube platforms, and visibility of their videos on your platform. Both are important concepts of PeerTube as they allow platforms and their users to interact.

::: tip What is a "follow"?
A follow is [a kind of activity](https://www.w3.org/TR/activitypub/#follow-activity-inbox) in the ActivityPub linguo. It allows to subscribe to a server's user activity in the PeerTube realm.
:::

### Following a platform

Following a PeerTube platform will display videos of that platform on your pages and your users will be able to interact with them.

You can discover other public platforms on https://instances.joinpeertube.org/instances.

You can add a PeerTube platform to follow and remove platforms you follow in *the Left Menu* -> `Administration` section -> `Settings` -> `Federation` -> `Following` -> `Follow` button, and add hostname of the platforms you want to follow there.

![image Adding servers to follow](/assets/admin-add-follow.jpg)


### Automatically follow other platforms

You can choose to automatically follow other PeerTube platforms in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Basic` by specifying a URL.
PeerTube will periodically get the URL and follow platforms from it.

The field accepts an URL that answers with a JSON object. So it could be a PeerTube index instance like https://instances.joinpeertube.org/api/v1/instances/hosts or a JSON file hosted on Github, Gitlab etc. See the [PeerTube index README](https://framagit.org/framasoft/peertube/instances-peertube) to know the JSON format.

We **encourage you to create your [own PeerTube index](https://framagit.org/framasoft/peertube/instances-peertube)**, that can be theme-based or moderated according to your rules. This way, we could imagine a group of PeerTube admins following the same index on which they only add platforms they trust.


### Being followed by a platform

Being followed will display videos of your PeerTube platform on your followers' pages, and their users will be able to interact with your videos.

You can choose to disable followers or manually approve them in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Basic` page, by (un)checking **Other platforms can follow yours** and **Manually approve new platform followers** boxes.


## Platforms redundancy

PeerTube has a built-in videos redundancy system, allowing an platform to duplicate and serve videos of another platform.
If your platform duplicates a video, web browser will randomly download segments of this video on the origin PeerTube platform and on your platform, relieving origin platform bandwidth.

### Enable this feature

To enable redundancy on your server, update your [production.yaml file](https://github.com/Chocobozzz/PeerTube/blob/develop/config/production.yaml.example) and uncomment the redundancy strategies your want.

You could for example choose to duplicate the most viewed or the recently added videos. If your PeerTube platform should act as a complete mirror for another platform, use the `recently-added` strategy and set the view count to 0.

Then, go on the web interface and choose which other followed platform you want to enable redundancy in *the Left Menu* -> `Administration` section -> `Settings` -> `Federation` -> `Following`.

![image showing following panel](/assets/enable-redundancy.jpg)

PeerTube will check regularly new videos to duplicate for every strategy (`check_interval` configuration key).
If the cache of a particular strategy is full, PeerTube will remove old videos and try to duplicate other ones (`min_lifetime` configuration key).

### Checklist to make redundancy work

If you are using the redundancy system to help other PeerTube platforms, make sure that your reverse proxy allows all origins
in CORS. This is of utmost importance, since otherwise clients will simply not be able to download parts of their videos
from you. Also make sure that your platform's response time and bandwidth are good.

### Control remote redundancy of your videos

Users of your platform will download segments of the video they watch from remote PeerTube platforms.
You may want to prevent or control which remote platform can mirror your videos to improve the privacy of your users.
To do this, update the `remote_redundancy.videos.accept_from` key of your [production.yaml file](https://github.com/Chocobozzz/PeerTube/blob/develop/config/production.yaml.example):
 * Set `nobody` to prevent remote platforms from mirroring your videos
 * Set `followings` to allow platforms your PeerTube platform follows to mirror your videos
 * Set `anybody` to allow any platform to mirror your videos


