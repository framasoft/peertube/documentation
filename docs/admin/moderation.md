# Moderate your PeerTube platform

## Administrators

As an administrator, you have full control over what platforms you follow, and which
users you welcome on your PeerTube platform. Some features have an impact over these two flows,
and can increase or decrease the need for moderation afterwards.
They are usually marked with a warning within the administration interface,
but here are some of them:
 * opening registrations (instead, you can answer to requests for accounts via the contact form, by requesting the new user to change their password)
 * leaving users to upload videos without limitation (instead, prefer a low quota or an automatic quarantine with a medium quota)
 * automatically follow PeerTube platforms (instead, host a list of platforms with like-minded admins)

## Moderators

When you create/update another user, you can put it as "moderator".
A moderator can:
 * Manage video blocks
 * Manage video abuses
 * Remove any video, channel, playlist or comment
 * Update any video
 * See all the videos (including unlisted and private videos)
 * Mute accounts/platforms for the entire PeerTube platform
 * Manage regular users (update, ban, delete)
 * List, accept and reject registration requests

## Video abuses

Any local or remote users can report a video with a particular "reason". All video abuses are listed in *the Left Menu* -> `Administration` section -> `Moderation` -> `Reports`. Admins and moderators can act on the video (updating/removing/blocking it for example).

Admins and moderators can:
 * Delete the report
 * Mark it as accepted/rejected. This action will also send a notification to the reporter.
 * Update the moderation comment (is only displayed to other admins/moderators)


## Mute accounts/platforms

See [https://docs.joinpeertube.org/use/mute](/use/mute)

You can also try the auto mute plugin: https://framagit.org/framasoft/peertube/official-plugins/-/tree/master/peertube-plugin-auto-mute
It automatically mutes accounts or PeerTube platforms depending on a public mute list (hosted on Github, Gitlab, pastebin etc).


## Videos blocks

### Manual block

Admins/moderators can block any videos (local or remote).
When you block a local video that is already federated, you can choose to **Unfederate**
it, meaning that the video will be removed from other PeerTube platforms.
Manually blocked videos are listed in *the Left Menu* -> `Administration` section -> `Moderation` -> `Video blocks`,
and are shown along regular video blocks unless you filter them out with the advanced block filters.

### Auto block

You can enable this feature in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Basic`.
When enabled, every new uploaded videos will be hidden by default until a moderator manually approves them.
Automatically blocked videos are listed in *the Left Menu* -> `Administration` section -> `Moderation` -> `Video blocks`,
and are shown along regular video blocks unless you filter them out with the advanced block filters.

You can then individually allow users you trust to bypass videos quarantine in *the Left Menu* -> `Administration` section -> `Overview` -> `Users` -> `Edit user`.


## List comments

To track video comments added on your PeerTube platform, go in *the Left Menu* -> `Administration` section -> `Overview` -> `Comments`.
This page will list comments added on your platform (by a remote platform or by a local user).
Then, you can:

 * Delete comments using `Actions > Delete this comment`
 * Delete all comments of an account using `Actions > Delete all comments of this account`

You also can see and filter comments auto-tagged by a watched list or comments with external link.

## Platform watched words lists

<Badge type="info" text="PeerTube >= 6.2"></Badge>

Video name/description and comments that contain any of the watched words are automatically tagged with the name of the list.
These automatic tags can be used to filter comments and videos.

To add a new list of watched words, you have to:

  * go in *the Left Menu* -> `Administration` section -> `Moderation` -> `Watched words`
  * click **Create a new list** button
  * add a **List name** and one word or group of words per line
  * click the **Save** button.

## Display private/unlisted videos

You can see private/unlisted or videos not yet published using the `Display all videos (private, unlisted or not yet published)` filter (behind the star wheel on the top right) of the following pages:
 * Local videos
 * Account videos
 * Channel videos


## Plugins

Framasoft developed two plugins to automatically mute or block accounts, PeerTube platforms or videos based on public blocklists:
 * [peertube-plugin-auto-mute](https://framagit.org/framasoft/peertube/official-plugins/-/tree/master/peertube-plugin-auto-mute)
 * [peertube-plugin-auto-block-videos](https://framagit.org/framasoft/peertube/official-plugins/-/tree/master/peertube-plugin-auto-block-videos)

You can install them using the admin interface.


## Tips

### Dealing with spam

With open registrations on your server, chances are you will have to deal with spam.
There is no simple and definitive way to solve this problem while keeping registrations open,
but there are several that could help you limit spam and its impact on your PeerTube platform.

Here are some things that could help:
 * **[Require signup approval](https://docs.joinpeertube.org/admin/managing-users#registration-approval) by moderators**
 * **Enable videos quarantine/auto video block**
 * **Disable new registrations:** you can disable new registrations, and update your platform description to explain that you accept new users on demand. They can for example use the contact button of the about page.
 * **Require email verification:** force email verification on signup in *the Left Menu* -> `Administration` section -> `Settings` -> `Configuration` -> `Basic`. Unfortunately this feature is not really useful since most spammers use a real email account (gmail etc) and automate the validation process.


### Understand what happens on your platform

To check what videos are uploaded on your server, or what your users do you can:
 * Check your [standard & audit logs](/admin/logs)
 * List all private/unlisted/public videos in the `Local` page, and then click on `Display all videos (private, unlisted or not yet published)`
 * Subscribe to your global videos feed (for example: https://peertube2.cpy.re/feeds/videos.xml?sort=-publishedAt&filter=local)
 * Subscribe to your global comments feed (for example: https://peertube2.cpy.re/feeds/video-comments.xml?sort=-publishedAt&filter=local)
