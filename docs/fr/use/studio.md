# Studio : modifiez vos vidéos

Après avoir téléversé une vidéo, vous avez la possibilité de la modifier succinctement depuis l'interface web de PeerTube avec **Studio**. Vous pouvez :

* couper la vidéo avec un nouveau début et/ou une nouvelle fin
* ajouter une vidéo d'introduction (un générique, par exemple)
* ajouter un vidéo de fin (les crédits, par exemple)
* ajouter un filigrane/logo (au format `png`, `jpg`, `jpeg` ou `webp`) à la vidéo

Pour ce faire, **une fois la vidéo publiée**, vous devez :

1. cliquer sur **…** sous votre vidéo
1. cliquer sur **Studio**
  ![image édition sous vidéo](../assets/FR_studio_vue_video.png)
1. faire vos modifications (la liste des modifications s'affichera sous la vignette de la vidéo)
1. cliquer sur **Lancer l'édition de la vidéo**

![image édition dans vidéo](../assets/FR_studio_edition.png)
