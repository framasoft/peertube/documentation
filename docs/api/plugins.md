# Plugins & Themes API

This is the API reference for plugins and themes. An introduction and quickstart into its use is provided in [the corresponding Contribute guide](/contribute/plugins).

## Hooks

### Server hooks (only plugins)

<<< @/remote-code/packages/models/src/plugins/server/server-hook.model.ts

### Client hooks

<<< @/remote-code/packages/models/src/plugins/client/client-hook.model.ts
<<< @/remote-code/packages/models/src/plugins/client/plugin-client-scope.type.ts


## Server register/unregister (only plugins)

Your library file should export a `register` and `unregister` functions:

<<< @/remote-code/server/core/types/plugins/plugin-library.model.ts

PeerTube provides different helpers to the `register` function:

<<< @/remote-code/server/core/types/plugins/register-server-option.model.ts

### Register hook options

To register hook listeners:

<<< @/remote-code/packages/models/src/plugins/server/register-server-hook.model.ts


### Register settings options

To register settings:

<<< @/remote-code/packages/models/src/plugins/server/settings/register-server-setting.model.ts
<<< @/remote-code/packages/models/src/plugins/client/register-client-form-field.model.ts

<<< @/remote-code/packages/models/src/plugins/server/managers/plugin-settings-manager.model.ts


### Storage manager API

To save/load JSON (please don't put too much data in there because we store it in the PeerTube database):

<<< @/remote-code/packages/models/src/plugins/server/managers/plugin-storage-manager.model.ts


### Register auth methods API

To register id and pass auth methods (LDAP etc), or external auth (OpenID, SAML2 etc) methods:

<<< @/remote-code/server/core/types/plugins/register-server-auth.model.ts



### Video categories manager API

<<< @/remote-code/packages/models/src/plugins/server/managers/plugin-video-category-manager.model.ts


### Video languages manager API

<<< @/remote-code/packages/models/src/plugins/server/managers/plugin-video-language-manager.model.ts


### Video licences manager API

<<< @/remote-code/packages/models/src/plugins/server/managers/plugin-video-licence-manager.model.ts

### Video privacy manager API

<<< @/remote-code/packages/models/src/plugins/server/managers/plugin-video-privacy-manager.model.ts

### Video playlist privacy manager API

<<< @/remote-code/packages/models/src/plugins/server/managers/plugin-playlist-privacy-manager.model.ts

### Video transcoding manager API

To add profile and encoders priority to ffmpeg transcoding jobs (profile needs to be selected by the admin in the PeerTube configuration):

<<< @/remote-code/packages/models/src/plugins/server/managers/plugin-transcoding-manager.model.ts

<<< @/remote-code/packages/models/src/videos/transcoding/video-transcoding.model.ts

## Client register

Your client script should export a `register` function:

<<< @/remote-code/client/src/types/client-script.model.ts

PeerTube provides different helpers to the `register` function:

<<< @/remote-code/client/src/types/register-client-option.model.ts


### Register hook options

To register hook listeners:

<<< @/remote-code/packages/models/src/plugins/client/register-client-hook.model.ts


### Register video form field options

<<< @/remote-code/packages/models/src/plugins/client/register-client-settings-script.model.ts


## Client plugin selectors

*Selector ids are prefixed by `plugin-selector-`. For example: `plugin-selector-login-form`*

<<< @/remote-code/packages/models/src/plugins/client/plugin-selector-id.type.ts

## Client placeholder elements

*Element ids are prefixed by `plugin-placeholder-`. For example: `plugin-placeholder-player-next`*

<<< @/remote-code/packages/models/src/plugins/client/plugin-element-placeholder.type.ts

## Client actions

<<< @/remote-code/packages/models/src/plugins/client/client-action.model.ts
