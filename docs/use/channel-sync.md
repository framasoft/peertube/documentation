# Automatically import videos a remote channel or playlist


## How to program a new import automation (synchronization)

If the administrator enabled this feature, you can automatically import all videos from a remote channel (from another video platform) into one of your PeerTube channels. This feature is also known as **channel synchronization**.

To do so, you have to:
1. go to **Channels** in *the Left Menu*;
2. click on **Synchronizations**;
3. click on **Add synchronization**;
4. then fill:
   - **Remote channel URL**: your Youtube / Dailymotion / Vimeo channel (*Note*: playlists URL are also allowed in this field)
   - **Video channel**: your PeerTube channel in which you will import videos from the remote channel;
   - **Options for existing videos on remote channel**: choose to import existing videos on remote channel if you will or to only watch for new publications;
5. click on **Create**


![Add a new synchronization button](/assets/add-synchronization.jpg)

![Add a new synchronization form](/assets/add-synchronization-form.jpg)

Once the synchronization is added, you should see the list of synchronizations updated:

![List synchronization](/assets/list-synchronizations.jpg)

Now wait for new publications. Note that by default, the remote channels will be checked each hours for new videos.

## Delete a synchronization

In the synchronization list, click on the **<vue-feather type="more-horizontal"></vue-feather>** button and select the **<vue-feather type="trash-2"></vue-feather> Delete** option:

![Delete synchronization](/assets/delete-sync.jpg)

## Update a synchronization

You may just delete the existing synchronization and recreate it using the above procedure. Ensure to check **Only watch for new publications** while creating the synchronization anew.
