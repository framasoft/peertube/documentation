# Publish a video or a live

## Upload a video

To publish a video you have to click the <vue-feather type="upload-cloud"></vue-feather>**Publish** button in *the Left Menu*. Once clicked you have 3 ways to upload a video:

1. [by selecting a file on your device](#upload-a-file);
2. [by importing an online video by its URL](#import-with-url);
3. [by importing an online video by its URI (torrent)](#import-with-torrent).

### Upload a file

Once you have to click the <vue-feather type="upload-cloud"></vue-feather>**Publish** button in the left menu:

1. select which channel you want to upload your video (can be done/change after upload);
2. select privacy settings for this video (can be done/change after upload);
3. click **Select file to upload** button.

While the video is uploading you can set [some details](#video-fields).

### Import with URL

If the administrator of your PeerTube platform enabled this option, you can import any URL [supported by youtube-dl](https://ytdl-org.github.io/youtube-dl/supportedsites.html) or URL that points to a raw MP4 file. To do so, you have to:

  1. click the <vue-feather type="upload-cloud"></vue-feather>**Publish** button in the left menu;
  2. click **Import with URL** tab;
  3. paste your video's url into **URL** field;
  4. select which channel you want to upload your video (can be done/change after upload);
  5. select privacy settings for this video (can be done/change after upload);
  6. click **Import** button.

::: warning
You should make sure you have diffusion rights over the content it points to, otherwise it could cause legal trouble to yourself and your PeerTube platform.
:::

While the video is uploading you can set [some details](#video-fields).

### Import with torrent

If the administrator of your PeerTube platform enabled this option, you can import any torrent file that points to a mp4 file. To do so, you have to:

1. click the <vue-feather type="upload-cloud"></vue-feather>**Publish** button in the left menu;
2. click **Import with torrent** tab;
3. select a `.torrent` file on your commputer or paste magnet URI of a video;
4. select which channel you want to upload your video (can be done/change after upload);
5. select privacy settings for this video (can be done/change after upload);
6. click **Import** button.

::: warning
You should make sure you have diffusion rights over the content it points to, otherwise it could cause legal trouble to yourself and your PeerTube platform.
:::

While the video is uploading you can set [some details](#video-fields).

## Publish a live

If the administrator of your PeerTube platform enabled this option, you can create a live using PeerTube and a streaming software (for example [OBS](https://obsproject.com/)). To do so, you have to:

1. click the <vue-feather type="upload-cloud"></vue-feather>**Publish** button in the left menu;
2. click **Go live** tab;
3. select which channel you want to publish your live;
4. select privacy settings for this live;
5. select if you want:
    * a **Normal live**: stream only once, replay will replace your live
    * a **Permanent/recurring live**: stream multiple times, replays will be separate videos
6. click **Go Live** button.

![user upload basic info - image](/assets/go-live-ui.jpg)

In the publication form, you have a **Live settings** tab that allows you to:

* See the **RTMP URL** to put in your streaming software
* See the live **stream key** associated to this live, to put in your streaming software. It is a private key, allowing anyone to stream a video in this live so don't share it with anyone!

Now you're ready to go live!

### Live examples

#### With OBS

In this example we'll use [OBS software](https://obsproject.com/) to send a stream to PeerTube, but you can use
any streaming software that can stream videos using the [RTMP protocol](https://en.wikipedia.org/wiki/Real-Time_Messaging_Protocol).

1. Open OBS on your computer
2. Click on **Settings** (or: **File** in top bar > **Settings**)
3. Click on **Streams** tab
4. Choose **Custom** service
5. Fill **Server** input using the PeerTube RTMP URL
6. Fill **Stream key** input using your live stream key
7. Do not use **authentication** and click on **OK**
8. Stream whatever your want and click on **Start streaming**. After some time, you'll be able to see your live in the PeerTube interface.

![obs settings image](/assets/live-obs-settings.jpg)

#### With Jitsi Meet

If you want to broadcast a webinar or a conference very easily, you can also use [Jitsi Meet](https://meet.jit.si) for recording the event, and rely on PeerTube for streaming it live. It is very useful if you have an expected audience too big for Jitsi to handle, but actually only a few people talking.

1. Create a chat room on a Jitsi server
2. Click on the **menu** (3-points icon at the bottom-right of the screen)
3. Click on **Start live stream**
4. You will be asked a Youtube live stream key. Actually, there is a way to make it work with PeerTube : simply paste the **PeerTube RTMP URL** followed by the **PeerTube live stream key** (you may have to add a slash in-between).
5. Click **Start live stream**, you may heard "The stream has started" and see your live on PeerTube after some time.

![Jitsi Start live stream menu](/assets/jitsi-start-live-stream.png)
![Jitsi window where inserting stream key](/assets/jitsi-insert-stream-key.png)

## Video fields

### Basic info

* **Title**: the name of the video (something more catchy than `myvideo.mp4` for example :wink: );
* **Tags**: tags can be used to suggest revelant recommandations. 5 tags maximum. You have to press enter to add one;
* **Description**: text you want to display above your video (supports markdown) - you can see how it looks above;
* **Channel**: in which channel you want to add your video;
* **Category**: what kind of content is your video (Activism? How to? Music?);
* **Licence**:
    * Attribution,
    * Attribution - Share Alike,
    * Attribution - No Derivatives,
    * Attribution - Non Commercial,
    * Attribution - Non Commercial - Share Alike,
    * Attribution - Non Commercial - No Derivatives,
    * Public Domain Dedication;
* **Language**: what is the main language in the video;
* **Privacy**: public, internal, unlisted, private. [See what it means](#video-confidentiality-options-what-do-they-mean);
* **Contains sensitive content?**: Some PeerTube platforms do not list videos containing mature or explicit content by default.

![user upload basic info - image](/assets/user-upload-video-basic-info.jpg)

### Captions

#### Automatic transcription <Badge type="info" text="PeerTube >= 6.2"></Badge>

If [enabled by the PeerTube platform admin](/admin/configuration#automatic-transcription), uploaded videos will automatically be subtitled. You still can edit or remove these subtitles after they are published.

#### Manual transcription

This tab allows you to add subtitles to your video. To add one, you have to:

1. go to **Captions** tab;
2. click <vue-feather type="plus"></vue-feather> **Add a caption** button;
3. select the caption language in the list;
4. click **Select the caption file** button;
5. browse into your computer file to select your `.vtt` or `.srt` file;
6. click **Add this caption** button;
7. click **<vue-feather type="check-circle"></vue-feather> Publish** / **<vue-feather type="check-circle"></vue-feather> Update** button.

Your caption is now available by clicking <vue-feather type="settings"></vue-feather> > **Subtitles/CC** and selecting the language.

To delete a caption you have to:

1. click **Delete** button in front of the language you want to delete;
2. click **<vue-feather type="check-circle"></vue-feather> Update** button.

#### Transcription widget

Users can open a transcription widget to display subtitles next to the video ([see here](../use/watch-video#transcription-widget)).

### Chapters <Badge type="info" text="PeerTube >= 6.0"></Badge>

To add chapters, you have to:

1. click on **…** under your video
1. click <vue-feather type="edit-2"></vue-feather> **Update**
1. click **Chapters** tab
1. add:
    * a timestamp (`H:MM:SS`) of the beginning of the chapter into **Timecode** field
    * a name for the chapter in **Chapter name** field
1. repeat as many times as necessary the steps above
1. click **<vue-feather type="check-circle"></vue-feather> Update** button

You can delete a chapter by clicking <vue-feather type="trash-2"></vue-feather> button in front of a chapter, and play the preview video to identify the timestamp.

Note that chapters can automatically be imported:
 * from a video container
 * from the external video platform when you import a video
 * from the video description using `timecode chaptername` format:
```
my super video description

00:00 chapter 1
00:05 my super chapter 2
10:05 awesome chapter 3

other description stuff
```

<iframe title="Peertube V6: chapters" src="https://framatube.org/videos/embed/6f0feeeb-cade-47d8-bfbf-a9a8504efdf3" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

### Advanced settings

This tab allows you:

* to edit your preview image by selecting an image from your device or select a frame of the video;
* to add a short text to tell people how they can support you (membership platform...);
* change the original publication date;
* to choose if you want:
    * to disable video comments
    * to enable comment (with approval if you set an [auto tag policy](../use/setup-account#auto-tag-policies))
    * that any new comment requires an approval
* disable/enable download of your video.

![comment policy video upload](/assets/comment-policy-video-upload.jpg)

::: info
Click **<vue-feather type="check-circle"></vue-feather> Update** button to save your new settings.
:::

### Video confidentiality options: what do they mean?

* **Public**: your video is public. Everyone can see it (by using search engine, link or embed);
* **Internal**: only authenticated user having an account on your PeerTube platform can see your video. Users searching from another PeerTube platform or having the link without being authentifaced can't see it;
* **Unlisted**: only people with the private link can see this video; the video is not visible without its link (can't be found by searching);
* **Private**: only you can see the video;
* **Password protected**: only users with the appropriate password can see this video; <Badge type="info" text="PeerTube >= 6.0"></Badge>
  <iframe title="Peertube V6 : password protection" src="https://framatube.org/videos/embed/e15b5e51-d603-41f4-b911-dcd88a651bc2" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>
* **Scheduled**: hide the video until a specific date, where the video becomes public.

## Upload a new version of a video <Badge type="info" text="PeerTube >= 6.0"></Badge>

::: info
This option has to [be enabled by the PeerTube platform administrator](/admin/configuration#videos).
:::

::: warning
Uploading a new version of your video will completely erase the current version!
:::

To upload a new version of a video, you have to:

1. click on the <vue-feather type="more-horizontal"></vue-feather> button under your video
2. click <vue-feather type="edit-2"></vue-feather> **Update**
3. click **Advanced settings** tab
4. click **Select the file to upload** button on **Replace video file** section
5. browse to find the new version of the video
6. click **Confim** button
7. click **<vue-feather type="check-circle"></vue-feather> Update** button.

The re-upload date is displayed under the video player.

<iframe title="PeerTube V6: reupload video" src="https://framatube.org/videos/embed/aeb01797-5adf-4297-90dc-c927c63eef08" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>
