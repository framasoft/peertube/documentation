# Muting other accounts/PeerTube platorm

## How to mute a PeerTube platform/account

As a user, you can hide other accounts content.
To mute an account, go on the account page (for example: https://peertube3.cpy.re/accounts/chocobozzz@peertube2.cpy.re) and click on <vue-feather type="more-horizontal"></vue-feather>.

Then, you can decide to:
 * Hide the account content
 * Hide the remote account's platform content

Mutes are listed in *the Header* -> <vue-feather type="settings"></vue-feather> button -> `Moderation` -> `Muted Accounts` or `Muted servers`.

If you are an administrator or a moderator, you can also:
 * Hide the account content for all the users of your PeerTube platform
 * Hide the remote account's platform content for all the users of your PeerTube platform

These mutes are listed in *the Left Menu* -> `Administration` section -> `Moderation` -> `Mutes` -> `Muted accounts` or `Muted servers`.

 ![](/assets/mute-account.png)

## What does mute do?

When you mute an account, all its content will be hidden:
 * Its videos won't be listed anymore
 * Its comments won't be displayed anymore
 * New comments by this PeerTube platform/account on your videos won't be forwarded to other platforms
