# Video statistics

You can find statistics about your videos in *the Left Menu* -> `My video space` section -> `Videos` -> <vue-feather type="more-horizontal"></vue-feather> button -> `Stats`.

![](/assets/stats/show-stats.jpg)

All these statistics aggregate watch session coming from your PeerTube platform, remote PeerTube platforms and from PeerTube embed integrated into external websites.

![](/assets/stats/video-stats.jpg)

You can also see a graph of views, watch time or countries (if your admin enabled this feature).

![](/assets/stats/video-graphs.jpg)
