# Report content

## With your PeerTube account

To report content while logged into your PeerTube account, you must, on the content page:

1. click <vue-feather type="more-horizontal"></vue-feather>
2. click <vue-feather type="flag"></vue-feather> **Report**
3. describe what's wrong with this content:
    * **Violent or repulsive**
    * **Hateful or abusive**
    * **Spam, ad or false news**
    * **Privacy breach or doxxing**
    * **Copyright**
    * **Breaks server rules**
    * **Thumbnails** (the issue is with the thumbnail)
    * **Captions** (the issue is with the captions)
4. *Optional*: if needed, you can point at the timestamp
5. describe the issue (remember that what is obvious for you may not be obvious to the person receiving the report)

::: info
If the content is hosted on another PeerTube platform, the report will be forwarded to this platform too.
:::

![image of report popup](/assets/report-modal.png)

## As a visitor

To report content while not logged into an account, you have to:

1. click in *the Left Menu* -> `More info` -> `Contact`
2. click **Contact us** button (if enabled by the admin)
3. fill all fields, describe the issue, and paste the content URL
