# Setup your account

## Create an account on PeerTube

To be able to upload a video, you must have an account on a _PeerTube platform_ (a server running PeerTube). The PeerTube project can help you to find a platform on the [JoinPeerTube website](https://joinpeertube.org/publish-videos) (note that PeerTube platform administrators must add themselves
to [this list](https://instances.joinpeertube.org) manually, it is not automatically generated).

Once you've found a platform that suits you, click "Create an account" button in the *Header*. (If you don't find the button, that means the PeerTube platform admin deactivated registration).

![Registration page after clicking on "Create an account"](/assets/profile-registration-01.jpg)

The second step of the registration process will show you the terms of use of the PeerTube platform you wish to register on. 

![Registration page, step 2](/assets/profile-registration-02.jpg)

You can then fill in your **login** (nickname), an **email address** and a **password**.

![Registration page, step 3](/assets/profile-registration-03.jpg)

Once this first step done, you can choose to setup your default channel by indicating its name (`id`) and display's name.

![Channel first creation](/assets/profile-registration-04.jpg)

## Connect to your PeerTube platform

To log in, you must go to the address of the particular platform that you registered on. PeerTube platforms share each other's videos, but
each platform's index of accounts is not federated. Click on the "Login" button in the top-left corner, then fill in your login/email address and
password. Note that the login name and password are **case-sensitive**!

Once logged in, your name and display name will appear below the platform name.

## Update your profile

To update your user profile, change your avatar, change your password, etc... Click on the <vue-feather type="settings"></vue-feather> button in the *Header*. You then have a few options:

* Settings
* Notifications
* Import/Export
* Applications
* Moderation
  * Muted accounts
  * Muted servers
  * Abuse reports

![user profile view](/assets/profile-library.jpg)

### Settings

**Settings** tab has several sections.

Before any of the other sections, you can:

* See/change your Avatar. The allowed filetypes and file-size should be displayed by clicking <vue-feather type="edit-2"></vue-feather> and move the mouse over the upload button.
* View your Upload Quota and used space. Your upload quota is determined by the PeerTube platform hosting your account. If your platform creates multiple versions of your videos in different qualities then all versions count towards your quota.

#### Profile

* View your Upload Quota and used space. Your upload quota is determined by the PeerTube platform that hosts your account. If your platform creates multiple versions of your videos in different qualities then all versions count towards your quota.
* Modify your **Display Name**. Note that your Display Name is not the same as your Username (used for logging in), which cannot be modified.
* Add or modify your **User Description**. This will be displayed on your Public Profile in the "About" section.

#### Interface

Change the web interface theme.


#### Video Settings

* Change how videos with sensitive content are displayed. You can choose to list them normally, blur their content, or hide them completely. This feature may be disabled on your PeerTube platform.
* Select which languages you want to see videos in. Languages which are not selected will be hidden. You can have up to 20 languages (or all languages) selected.
* Select if you want to help sharing videos being played or not (by using, or not, P2P).
* Change whether videos autoplay or not.
* Change whether you want to automatically start next video or not.

#### Notifications

Change which notifications you receive. You can choose to receive notifications in PeerTube, via Email, both, or neither for:

  * New video from your subscriptions
  * New comment on your video
  * **admin/moderator** New abuse
  * **admin/moderator** Video blocked automatically waiting review
  * One of your video is blocked/unblocked
  * Video published (after transcoding/scheduled update)
  * Video import finished
  * **admin/moderator** A new user registered on your platform
  * You or your channel(s) has a new follower
  * Someone mentioned you in video comments
  * **admin/** Your platform has a new follower
  * **admin/** Your platform automatically followed another PeerTube platform
  * An abuse report received a new message
  * One of your abuse reports has been accepted or rejected by moderators

When **Web** notifications are enabled, there are visible above your avatar in the *Header*:

![notification number](/assets/use-settings-notifications-indicator.jpg)

By clicking it you can see your notification list. You can click each notification, or:

  * <vue-feather type="inbox"></vue-feather> mark all as read
  * <vue-feather type="settings"></vue-feather> update your notifications preferences

#### Password

Change your Password. To change your password you will need to enter your current password, and then enter your new password twice before clicking **_Change Password_**.

#### Two-factor authentication

Add two-factor authentication to your account.

#### Email

Change your Email. You will need to enter your password again to change this setting.

#### Danger Zone

 Delete your account.

::: danger
**This is irreversible. This will delete all your data, including channels, videos and comments. Content cached by other servers and other third-parties might make longer to be deleted.**

You need to confirm your username to proceed. If you delete your account, you won't be able to create another with the same username on the PeerTube platform!
:::

### Account import / export <Badge type="info" text="PeerTube >= 6.1"></Badge>

::: info
Since 6.1 version of PeerTube you can export an account from a platform to another one. However, this is not a migration (yet)!
:::

#### Account export <Badge type="info" text="PeerTube >= 6.1"></Badge>

You can request an archive of your account containing:

 * Your account settings with avatar file
 * Your channels with banner and avatar files
 * Your muted accounts and servers
 * Your comments
 * Your likes and dislikes
 * Your subscriptions and followers
 * Your video playlists with thumbnail files
 * Your videos with thumbnail, caption files. Video files can also be included in the archive
 * Your video history

The exported data will contain multiple directories:

 * A directory containing an export in ActivityPub format, readable by any compliant software
 * A directory containing an export in custom PeerTube JSON format that can be used to re-import your account on another PeerTube platform
 * A directory containing static files (thumbnails, avatars, video files etc.)

You can only request one archive at a time.

An email will be sent when the export archive is available.

To **export** your account, you have to click:

  1. in *the Header* -> <vue-feather type="settings"></vue-feather> button -> `Import/Export`
  1. scroll down to **EXPORT** section and click the **Request a new archive** button.

![export an account view image](/assets/export_account.jpg)

Once done, you will have to click **Include video files in archive file** (including video files is required if you want to re-import your videos on another PeerTube website) and click the **Request an archive** button.

![image popup archive request](/assets/export_archive_popup.jpg)

You will be notyfied by email and will see your archive by refreshing the page:

![image list of archives](/assets/list_export_archives.jpg)

#### Account import <Badge type="info" text="PeerTube >= 6.1"></Badge>

You can import an archive created by another PeerTube website.

This is an import tool and not a migration tool. It's the reason why data (like channels or videos) is duplicated and not moved from your previous PeerTube website.

The import process will automatically:

 * Update your account metadata (display name, description, avatar...)
 * Update your user settings (autoplay or P2P policy, notification settings...). It does not update your user email, username or password
 * Add accounts/servers in your mute list
 * Add likes/dislikes
 * Send a follow request to your subscriptions
 * Create channels if they do not already exist
 * Create playlists if they do not already exist
 * Add watched videos in your video history
 * If the archive contains video files, create videos if they do not already exist

The following data objects are not imported:

 * Comments
 * Followers (accounts will need to re-follow your channels)

An email will be sent when the import process is complete.

To **import** your account, you have to click:

  1. in *the Header* -> <vue-feather type="settings"></vue-feather> button -> `Import/Export`
  2. scroll down to **IMPORT** section and click the **Select the archive file to import** button.

![import an account view image](/assets/import_account.jpg)


### Moderation

#### Comments

You can find out all comments posted on your videos on the dedicated page. In *the Left Menu* -> `My video space` section -> `Videos` -> `Comments`. You can easily filter, approve or remove comments:

![comments on videos screenshot](/assets/comments-on-videos-screenshot.jpg)

:::info
Admin/moderators of the PeerTube platform, owner of the video and the person that writes the comment can see the pending one.
:::

![image pending review comment](/assets/comment-pending-review.jpg)

#### Watched words <Badge type="info" text="PeerTube >= 6.2"></Badge>

In the `Watched Words` page, you can define list of words to watch.
Comments that contain any of the watched words are automatically tagged with the name of the list.
These automatic tags can be used to filter comments or automatically block them.

![watched words list](/assets/watched-words.jpg)

#### Auto tag policies <Badge type="info" text="PeerTube >= 6.2"></Badge>

In the `Auto tag policies` page, you can setup how auto tag policies should work.
You can so automatically block comments based on [your watched lists](#watched-words) and/or that contain an external link (to prevent spammers for instance).

![auto tag policies screenshot](/assets/auto-tag-policies.jpg)
