# Watch, share, download a video

## Watching a video

![image player interface](/assets/video-player-watch.png)

The player interface is composed of:

  1. <vue-feather type="play"></vue-feather> and <vue-feather type="pause"></vue-feather> button to play or pause the video
  1. <vue-feather type="skip-forward"></vue-feather> button to go to the next video
  1. time indicator: elapsed time/total
  1. P2P statistics (if enabled; if not you see **HTTP** instead)
  1. <vue-feather type="volume-2"></vue-feather> or <vue-feather type="volume-x"></vue-feather> to mute/unmute soundtrack
  1. volume controls: you can use your keyboard arrow to increase or decrese volume when you first click the icon
  1. quick enable/disable subtitles
  1. <vue-feather type="settings"></vue-feather> settings: to change the video quality, speed and subtitles
  1. theater or normal mode to enlarge the display
  1. full screen mode

### Storyboard <Badge type="info" text="PeerTube >= 6.0"></Badge>

You can see a live preview of the image around the target timecode as you move the progress bar.

<iframe title="PeerTube V6: storyboard" src="https://framatube.org/videos/embed/73556243-7a9b-496c-a740-f80e42ee0ad9" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

### Transcription widget <Badge type="info" text="PeerTube >= 6.3"></Badge>

Users can open the transcription widget that appears next to the player. They also can search the transcript and click on a specific segment to automatically jump the video to the appropriate timecode. The transcription is in sync with the video.

To display this widget you have to:

1. click <vue-feather type="more-horizontal"></vue-feather> button under your video
1. click **Show transcription**

**This option is only displayed if subtitles are available for the video**.

![gif to display the transcription widget](/assets/transcription_widget_show.gif)

## Sharing a video

To share a video, you first have to go to the video page you want to share.

Once on the video page, you just have to click the <vue-feather type="share-2"></vue-feather>__SHARE__ button, and are presented with a few options:

::: tip
The small icons at the end of the line allow you to copy the whole URL at once.
:::

### URL

An URL to the video (i.e.: `https://framatube.org/videos/watch/9db9f3f1-9b54-44ed-9e91-461d262d2205`). It can be sent to your contact the way you want; they will be able to access the video directly.

![Modal presenting options to share a video](/assets/video-share-modal.png)

### QR code

A QR code to facilitate sharing.

![Modal QR cdoe img](/assets/video-share-modal-qr-code.png)

### Embed

An embed code that allows you to insert a video player in your website.

![Modal embed img](/assets/video-share-modal-embed.png)

### OEmbed

When a user shares a PeerTube link, the application exposes information using the [OEmbed](https://oembed.com/) format allowing external websites to automatically embed the video (i.e inject the PeerTube player). Some platforms do this with any external link (Twitter, Mastodon...) but some other platforms may need additional configuration.

If your application supports OEmbed (for example [Moodle](https://moodle.org/) with the [Oembed Filter plugin](https://moodle.org/plugins/filter_oembed)), you could add the following code snippet in the OEmbed administration configuration to automatically embed PeerTube videos:

```json
[
  {
    "schemes":["https:\/\/instance.tube\/videos\/watch\/*"],
    "url":"https:\/\/instance.tube\/services\/oembed",
    "discovery":true
  }
]
```

### More options

For each option, you can:

  * set a time to start by clicking **Start at** and change the timestamp
  * if there are subtitles, choose to display one by default by clicking **Auto select subtitle**

You also have the possibility to customize a little more by clicking **More customization** button:

![More customization modal image](/assets/video-share-modal-more.png)

  * **Start at**: choose the timestamp you want to start the video;
  * **Stop at**: choose the timestamp you want to stop the video;
  * **Autoplay**: click if you want to start the video automatically;
  * **Muted**: click if you want the video to be played without sound (can be undone by user during watching);
  * **Loop**: click if you want the video to be repeated;
  * **Display video title** (only for **Embed**): unclick if you don't want to display the title of the video;
  * **Display privacy warning** (only for **Embed**): unclick if you don't want to display **Watching this video may reveal your IP address to others.** warning message;
  * **Display player control bar** (only for **Embed**): unclick if you don't want to display control bar buttons.

## Download a video

You can download videos directly from the web interface of PeerTube, by clicking the <vue-feather type="download"></vue-feather> **Download** button below the video:

![Modal presenting options to download a video](/assets/video-share-download.png)

A popup should appear with default values: direct download of the best quality video. You just have to click **Download** button to actually download the video on your computer.

By clicking the <vue-feather type="chevron-down"></vue-feather> **Advanced** button, you can find detailled informations about the video like its format, video and audio stream.

![advanced download popup](/assets/use-download-modal-advanced.gif)

You also have the possibility to download the video using torrent:

 * **Direct Download**: your web browser downloads the video from the origin server of the video. <br />
 * **Torrent (.torrent file)**: you need a WebTorrent compatible client to download the video not only from the origin server but also from other peers downloading the video

::: info
Depending on the PeerTube platform, you can download the video in different formats. However, please make sure you are granted a licence compatible with the planned usage of the video beforehand.
:::

## Settings for anonymous users

As an anonymous user you can customize your interface too. To do so you have to click <vue-feather type="settings"></vue-feather> button in the *Header*.

### Display settings

  * **Default policy on videos containing sensitive content:**
    * Do not list
    * Blur thumbnails
    * Display

    ::: info
    With Do not list or Blur thumbnails, a confirmation will be requested to watch the video.
    :::
  * **Only display videos in the following languages/subtitles**: select which language you want to display in *Browse videos* and *Search* pages.

### Video settings

  * **Help share videos being played**: the sharing system implies that some technical information about your system (such as a public IP address) can be sent to other peers, but greatly helps to reduce server load;
  * **Automatically play videos**: when on a video page, directly start playing the video;
  * **Automatically start playing the next video**: When a video ends, follow up with the next suggested video.

### Interface settings

To change the platform theme.

## Keyboard Shortcuts

You can use shortcuts to do some actions. To display the help menu, you can either click your avatar and click **<vue-feather type="command"></vue-feather> Keyboard shortcuts** or click **Keyboard shortcuts** at the end of left hand menu if you're not connected to your account.
You also can use `?`.

  * `?`	Show / hide this help menu
  * `esc`	Hide this help menu
  * `/s`	Focus the search bar
  * `b`	Toggle the left menu
  * `g o`	Go to the *Discover videos* page
  * `g v` Go to the *Browse videos* page
  * `g u`	Go to the *Publish video* page
  * `f`	Enter/exit fullscreen (requires player focus)
  * `space`	Play/Pause the video (requires player focus)
  * `m`	Mute/unmute the video (requires player focus)
  * `0-9`	Skip to a percentage of the video: 0 is 0% and 9 is 90% (requires player focus)
  * `↑`	Increase the volume (requires player focus)
  * `↓`	Decrease the volume (requires player focus)
  * `→`	Seek the video forward (requires player focus)
  * `←`	Seek the video backward (requires player focus)
  * `>`	Increase playback rate (requires player focus)
  * `<`	Decrease playback rate (requires player focus)
  * `.`	Navigate in the video frame by frame (requires player focus)
