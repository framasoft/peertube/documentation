# Search

There are multiple ways to make a search on PeerTube. The administrator can enable or disable each one.


## Platform search

In this mode, the search will only display results known by your PeerTube platform (so videos and channels of PeerTube platforms followed by your current platform).


## Global search

*Has to be enabled by your admin*

![Search](/assets/use-search-popup.jpg)

In this mode, your PeerTube platform will use an external global index to make a search. The results are not scoped to videos and channels
known by your platform. For example, you can search videos of PeerTube platforms that are not followed by your current platform.


## URI/handle search

*Can be disabled by your admin*

You can search a specific remote video that may not be federated with your PeerTube platform using its URL (for example: https://peertube.cpy.re/videos/watch/da2b08d4-a242-4170-b32a-4ec8cbdca701).

You can also search a specific remote channel using its URL or the channel handle (for example: https://peertube.cpy.re/accounts/chocobozzz or `chocobozzz@peertube.cpy.re`).
