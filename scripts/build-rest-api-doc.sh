#!/bin/sh

rm -f ./docs/public/api-rest-reference.html

curl -s https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/support/doc/api/openapi.yaml --create-dirs -o /tmp/peertube-openapi.yaml

node_modules/.bin/redocly build-docs /tmp/peertube-openapi.yaml \
  --theme.openapi.theme.colors.primary.main="#f48a44" \
  --theme.openapi.nativeScrollbars=true \
  --theme.openapi.requiredPropsFirst=true \
  --theme.openapi.sortPropsAlphabetically=true \
  --disableGoogleFont \
  -o docs/public/api-rest-reference.html

rm /tmp/peertube-openapi.yaml
